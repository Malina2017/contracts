﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppContracts
{
    /// <summary>
    /// Логика взаимодействия для EditTypeView.xaml
    /// </summary>
    public partial class EditTypeView : Window
    {
         public bool DialogResult { get; internal set; }
        public string Title { get; internal set; }
        public Type DataContext { get; internal set; }

        public EditTypeView()
        {
            InitializeComponent();
        }

        private void btOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        internal void ShowDialog()
        {
            throw new NotImplementedException();
        }
    }
}

