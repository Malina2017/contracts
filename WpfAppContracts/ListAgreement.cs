﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppContracts
{
    class ListAgreement : ObservableCollection<Agreement>
    {
        public ListAgreement()
        {
            using (var context = new ContractsEntities())
            {
                var query = from agreement in context.Agreements
                            orderby agreement.Number
                            select agreement;
                foreach (var c in query)
                    this.Add(c);
            }
        }
    }
}
