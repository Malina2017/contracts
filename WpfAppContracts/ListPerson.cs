﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace WpfAppContracts
{
    class ListPerson : ObservableCollection<Person>
    {
        public ListPerson()
        {
            using (var context = new ContractsEntities())
            {
                var query = from person in context.People
                            orderby person.Inn
                            select person;
                foreach (var c in query)
                    this.Add(c);
            }
        }
    }
}
