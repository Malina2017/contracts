﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace WpfAppContracts
{
    class ListStatus : ObservableCollection<Status>
    {
        public ListStatus()
        {
            using (var context = new ContractsEntities())
            {
                var query = from status in context.Status
                            orderby status.Status1
                            select status;
                foreach (var c in query)
                    this.Add(c);
            }
        }
    }
}
