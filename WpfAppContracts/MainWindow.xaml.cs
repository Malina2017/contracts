﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;


namespace WpfAppContracts
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        ObservableCollection<Agreement> agreements;
        ObservableCollection<Person> persons;
        ObservableCollection<Status> statuss;
        ObservableCollection<Type> types;


        public MainWindow()
        {
            InitializeComponent();
        }
        /// Загрузка  данных по договору

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ContractsEntities())
            {
                var query = from agreement in context.Agreements
                            select agreement;
                if (query.Count() != 0)
                {
                    agreements = new ObservableCollection<Agreement>();
                    foreach (var c in query)
                        agreements.Add(c);

                    lvAgreement.ItemsSource = agreements;
                }
            }
        }
        /// Загрузка  данных по клиентам
        private void btnLoad1_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ContractsEntities())
            {
                var query = from person in context.People
                            select person;
                if (query.Count() != 0)
                {
                    persons = new ObservableCollection<Person>();
                    foreach (var c in query)
                        persons.Add(c);

                    lvPerson.ItemsSource = persons;
                }
            }

        }
        /// Загрузка  данных по статусу договора
        private void btnLoad2_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ContractsEntities())
            {
                var query = from status in context.Status
                            select status;
                if (query.Count() != 0)
                {
                    statuss = new ObservableCollection<Status>();
                    foreach (var c in query)
                        statuss.Add(c);

                    lvStatus.ItemsSource = statuss;
                }
            }
        }
        /// Загрузка  данных по типу

        private void btnLoad3_Click(object sender, RoutedEventArgs e)
        {
            GetTypeAgreement();
        }
        /// Получение данных по типу
        private void GetTypeAgreement()
        {
            using (var context = new ContractsEntities())
            {
                var query = from type in context.Types
                            orderby type.Type1
                            select type;
                if (query.Count() != 0)
                {
                    types = new ObservableCollection<Type>();
                    foreach (var o in query)
                        types.Add(o);

                    lvType.ItemsSource = types;
                }
            }
        }
        //удаление по договору
        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            Agreement delAgreement = (Agreement)lvAgreement.SelectedItem;
            using (var context = new ContractsEntities())
            {
                Agreement delAgre = context.Agreements.Find(delAgreement.Id);
                if (delAgre != null)
                {
                    MessageBoxResult result = MessageBox.Show("Удалить данные по договору: \n" + delAgre.Id, "Предупреждение", MessageBoxButton.OKCancel);
                    if (result == MessageBoxResult.OK)
                    {
                        try
                        {
                            context.Agreements.Remove(delAgre);
                            context.SaveChanges();
                            agreements.Remove(delAgreement);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("\nОшибка удаления данных!\n" + ex.Message, "Предупреждение");
                        }
                    }
                }
            }
        }
        // Добавление новый договор
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Agreement newAgreement = new Agreement();
            EditAgreementView editAgreementView = new EditAgreementView();
            editAgreementView.DataContext = newAgreement;
            editAgreementView.Title = "Создать новый договор";
            editAgreementView.ShowDialog();
            if (editAgreementView.DialogResult == true)
            {
                using (var context = new ContractsEntities())
                {
                    try
                    {
                        Agreement agre = context.Agreements.Add(newAgreement);
                        context.SaveChanges();
                        EditAgreementView.GetAgreement();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка добавления данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }

        }
        //редактировать по договору
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Agreement editAgre = (Agreement)lvAgreement.SelectedItem;

            EditAgreementView editAgreementView = new EditAgreementView();
            editAgreementView.Title = "Редактирование данных по клиенту";
            editAgreementView.DataContext = editAgre;
            editAgreementView.ShowDialog();

            if (editAgreementView.DialogResult == true)
            {
                using (var context = new ContractsEntities())
                {
                    Agreement agre = context.Agreements.Find(editAgre.Id);
                    if (agre.Number != editAgre.Number)
                        agre.Number = editAgre.Number.GetValueOrDefault();
                    if (agre.DataOpen != editAgre.DataOpen)
                        agre.DataOpen = editAgre.DataOpen.GetValueOrDefault();
                    if (agre.DataClouse != editAgre.DataClouse)
                        agre.DataClouse = editAgre.DataClouse.GetValueOrDefault();

                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка редактирования данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }
        }
        //редактировать по клиенту
        private void btnEdit1_Click(object sender, RoutedEventArgs e)
        {
            Person editPerson = (Person)lvPerson.SelectedItem;

            EditPersonView editPersonView = new EditPersonView();
            editPersonView.Title = "Редактироание данных по клиенту";
            editPersonView.DataContext = editPerson;
            editPersonView.ShowDialog();

            if (editPersonView.DialogResult == true)
            {
                using (var context = new ContractsEntities())
                {
                    Person pers = context.People.Find(editPerson.Id);

                    if (pers.Inn != editPerson.Inn)
                        pers.Inn = editPerson.Inn.GetValueOrDefault();
                    if (pers.Type != editPerson.Type)
                        pers.Type = editPerson.Type.Trim();
                    if (pers.Shifer != editPerson.Shifer)
                        pers.Shifer = editPerson.Shifer.GetValueOrDefault();
                    if (pers.Data != editPerson.Data)
                        pers.Data = editPerson.Data.GetValueOrDefault();
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка редактирования данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }
        }
        //редактировать по статусу договора
        private void btnEdit2_Click(object sender, RoutedEventArgs e)
        {
            Status editStatus = (Status)lvStatus.SelectedItem;

            EditStatusView editStatusView = new EditStatusView();
            editStatusView.Title = "Редактироание данных по статусу";
            editStatusView.DataContext = editStatus;
            editStatusView.ShowDialog();

            if (editStatusView.DialogResult == true)
            {
                using (var context = new ContractsEntities())
                {
                    Status stat = context.Status.Find(editStatus.Id);

                    if (stat.Status1 != editStatus.Status1)
                        stat.Status1 = editStatus.Status1.Trim();

                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка редактирования данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }
        }
        //редактировать по типу договора

        private void btnEdit3_Click(object sender, RoutedEventArgs e)
        {
            Type editType = (Type)lvType.SelectedItem;

            EditTypeView editTypeView = new EditTypeView();
            editTypeView.Title = "Редактирование данных по типу";
            editTypeView.DataContext = editType;
            editTypeView.ShowDialog();

            if (editTypeView.DialogResult == true)
            {
                using (var context = new ContractsEntities())
                {
                    Type type = context.Types.Find(editType.Id);
                    if (type.Type1 != editType.Type1)
                        type.Type1 = editType.Type1.Trim();


                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка редактирования данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }
        }
        //добавление по клиенту
        private void btnAdd1_Click(object sender, RoutedEventArgs e)
        {
            Person newPerson = new Person();
            EditPersonView editPerson = new EditPersonView();
            editPerson.Title = "Добавление данных по клиенту";
            editPerson.DataContext = newPerson;
            editPerson.ShowDialog();

            if (editPerson.DialogResult == true)
            {
                using (var context = new ContractsEntities())
                {
                    try
                    {


                       
                        context.People.Add(newPerson);
                        context.SaveChanges();
                        persons.Add(newPerson);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка добавления данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }

        }
        //добавление по статусу
        private void btnAdd2_Click(object sender, RoutedEventArgs e)
        {
            Status newStatus = new Status();
            EditStatusView editStatus = new EditStatusView();
            editStatus.Title = "Добавление данных по статусу";
            editStatus.DataContext = newStatus;
            editStatus.ShowDialog();

            if (editStatus.DialogResult == true)
            {
                using (var context = new ContractsEntities())
                {
                    try
                    {
                        context.Status.Add(newStatus);
                        context.SaveChanges();
                        statuss.Add(newStatus);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка добавления данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }
        }
        //добавление по типу
        private void btnAdd3_Click(object sender, RoutedEventArgs e)
        {
            Type newType = new Type();
            EditTypeView editType = new EditTypeView();
            editType.Title = "Добавление данных по типу договора";
            editType.DataContext = newType;
            editType.ShowDialog();

            if (editType.DialogResult == true)
            {
                using (var context = new ContractsEntities())
                {
                    try
                    {
                        context.Types.Add(newType);
                        context.SaveChanges();
                        types.Add(newType);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка добавления данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }
        }
        //удаление по клиенту
        private void btnDel1_Click(object sender, RoutedEventArgs e)
        {
            Person delPerson = (Person)lvPerson.SelectedItem;
            using (var context = new ContractsEntities())
            {
                Person delPers = context.People.Find(delPerson.Id);
                if (delPers != null)
                {
                    MessageBoxResult result = MessageBox.Show("Удалить данные по клиенту: \n" + delPers.Id, "Предупреждение", MessageBoxButton.OKCancel);
                    if (result == MessageBoxResult.OK)
                    {
                        try
                        {
                            context.People.Remove(delPers);
                            context.SaveChanges();
                            persons.Remove(delPerson);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("\nОшибка удаления данных!\n" + ex.Message, "Предупреждение");
                        }
                    }
                }
            }
        }
        //удаление по статусу
        private void btnDel2_Click(object sender, RoutedEventArgs e)
        {
            Status delStatus = (Status)lvStatus.SelectedItem;
            using (var context = new ContractsEntities())
            {
                Status delStat = context.Status.Find(delStatus.Id);
                if (delStat != null)
                {
                    MessageBoxResult result = MessageBox.Show("Удалить данные по статусу: \n" + delStat.Id, "Предупреждение", MessageBoxButton.OKCancel);
                    if (result == MessageBoxResult.OK)
                    {
                        try
                        {
                            context.Status.Remove(delStat);
                            context.SaveChanges();
                            statuss.Remove(delStatus);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("\nОшибка удаления данных!\n" + ex.Message, "Предупреждение");
                        }
                    }
                }
            }
        }
        //удаление по типу договора
        private void btnDel3_Click(object sender, RoutedEventArgs e)
        {
            Type delType = (Type)lvStatus.SelectedItem;
            using (var context= new ContractsEntities())
            {
                Type delTyp = context.Types.Find(delType.Id);
                if (delTyp != null)
                {
                    MessageBoxResult result = MessageBox.Show("Удалить данные по типу договора: \n" + delType.Id, "Предупреждение", MessageBoxButton.OKCancel);
                    if (result == MessageBoxResult.OK)
                    {
                        try
                        {
                            context.Types.Remove(delTyp);
                            context.SaveChanges();
                            types.Remove(delType);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("\nОшибка удаления данных!\n" + ex.Message, "Предупреждение");
                        }
                    }
                }
            }
        }
    }
}
    
    
    
    
    
    












   